### Create with curl

```bash
curl -X POST -H 'Content-Type: application/json' \
-H 'Authorization: Bearer '$TOKEN'' \
-d '{   "name":"deployment-server",
        "size":"s-1vcpu-512mb-10gb",
        "region":"sfo3",
        "image":"ubuntu-22-04-x64",
        "vpc_uuid":"'${$VPC_UUID}'"}' \
    "https://api.digitalocean.com/v2/droplets"
```

### Create with cli

```bash
doctl compute droplet create \
--image ubuntu-22-04-x64 \
--size s-1vcpu-512mb-10gb \
--region sfo3 \
--vpc-uuid $VPC_UUID \
deployment-server
```
