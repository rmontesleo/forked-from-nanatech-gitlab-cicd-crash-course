### connect with the server

```bash
ssh -i ${SSH_PRIVATE_KEY} root@${DROPLET_IP}

# inside the server
apt update
apt  install docker.io -y

# update the server and install nginx
apt update -y && apt install nginx -y

# create some tomcat instances
docker run  -d  -p 8080:8080 tomcat:9.0
docker run  -d  -p 8081:8080 tomcat:9.0
docker run  -d  -p 8082:8080 tomcat:9.0

```
