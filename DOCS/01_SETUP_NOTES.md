```bash

# 1:
 git clone git@gitlab.com:rmontesleo/forked-from-nanatech-gitlab-cicd-crash-course.git

# 2:
cd forked-from-nanatech-gitlab-cicd-crash-course

# 3: use the configuration for devcontainers

# 4: upgrade pip
python3 -m pip install --upgrade pip

# 5: verify pip version
pip --version

# 6: clean environment
make clean

# 7: execute test
make test

## 8: if test fail, verify all your requirements.txt files are completed
```

### 9: requirements file

```bash
Flask==2.1.0
py-cpuinfo==7.0.0
psutil==5.8.0
gunicorn==20.1.0
black==20.8b1
flake8==3.9.0
pytest==6.2.2
Werkzeug==2.2.2
```

```bash
# 10:
make test

# 11:
make run

```
